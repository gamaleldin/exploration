====================================================================
This directory is for files (typically pdfs) that contain literature
or other external informational resources that are relevant to this
project.  This is typically all the papers and talks that I find in 
the course of researching this project. 

Note that the .gitignore file in this dir means that all contents
of this directory will be ignored in git.  You can override this with -f 
when you do 'git add'.  However, you should not really ever do this. 
Version control systems are for tracking changes to code and notes,
*not* for backup of large binary files that can not be diff'ed.  If 
it is important to have a binary file in the repository, please add
it to the "files" tab in assembla (then you can link to it in your
tickets).  That will not be tracked in git, but it will be available.
Typically however you should just leave it out.  One example of a 
reasonable exception is if you really want a paper to be read by 
everyone related to this project.  In that case, upload it in "files"
in assembla, and make tickets linking to that paper and asking others
to read it there. 	

Note: you will probably want to 'git rm README' from this subdirectory
(*not* from the project root directory; that one is important for 
explaining the functionality of the package).  Removing this file will
respect the structure of the repo; it is here presently only to get 
you started.
====================================================================


