#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 17 13:37:41 2017

@author: Gamaleldin Elsayed
"""

#%% imports
import tensorflow as tf
import numpy as np
from numpy import random as rng

#%%

class lstm_net:
    def __init__(self, input_size, net_size, out_size, gate = tf.nn.sigmoid, activation = tf.nn.tanh, activation_out = tf.nn.softmax, dtype=tf.float32):
        self.dtype = dtype;
        self.input_size = input_size;
        self.net_size = net_size;
        self.out_size = out_size;
        self.gate = gate;
        self.activation = activation;
        self.activation_out = activation_out;
        ''' layer 1: forget gate weights and biases'''
        self.Wf = tf.Variable(initial_value=tf.truncated_normal(shape = [net_size, net_size+input_size], stddev=1./np.sqrt(net_size), dtype= dtype), name = 'Wf'); 
        self.bf = tf.Variable(initial_value=tf.zeros(shape = [net_size, 1], dtype= dtype), name = 'bf'); 
        
        ''' layer 2: input gate weights and biases'''
        self.Wi = tf.Variable(initial_value=tf.truncated_normal(shape = [net_size, net_size+input_size], stddev=1./np.sqrt(net_size), dtype= dtype), name = 'Wi'); 
        self.bi = tf.Variable(initial_value=tf.zeros(shape = [net_size, 1], dtype= dtype), name = 'bi'); 

        ''' layer 3: new candidate weights and biases'''
        self.Wc = tf.Variable(initial_value=tf.truncated_normal(shape = [net_size, net_size+input_size], stddev=1./np.sqrt(net_size), dtype= dtype), name = 'Wc'); 
        self.bc = tf.Variable(initial_value=tf.zeros(shape = [net_size, 1], dtype= dtype), name = 'bc'); 

        ''' layer 4: hidden weights and biases'''
        self.Wh = tf.Variable(initial_value=tf.truncated_normal(shape = [net_size, net_size+input_size], stddev=1./np.sqrt(net_size), dtype= dtype), name = 'Wh'); 
        self.bh = tf.Variable(initial_value=tf.zeros(shape = [net_size, 1], dtype= dtype), name = 'bh'); 

        ''' read-out/output layer: weights and biases'''
        self.Wo = tf.Variable(initial_value=tf.truncated_normal(shape = [out_size, net_size], stddev=1./np.sqrt(net_size), dtype= dtype), name = 'Wo'); 
        self.bo = tf.Variable(initial_value=tf.zeros(shape = [out_size, 1], dtype= dtype), name = 'bo'); 
        
        ''' state of the net at initialization '''
        self.h = tf.zeros(shape = [net_size, 1]);  # hidden state
        self.c = tf.zeros(shape = [net_size, 1]);  # new candidate state
        self.o = tf.zeros(shape = [out_size]);  # output state
        
    
    #####################################################################################################################
    ''' update_state: updates the state of the network one time step '''
    #####################################################################################################################
    def update_state(self, x):
        x = tf.cast(x, dtype = self.dtype);
        hx = tf.pack(tf.concat(0, [tf.squeeze(self.h), tf.squeeze(x)])[:, tf.newaxis]); # concatinate the previous hidden state and the current input
        ''' gates new state '''
        fg = self.gate(tf.matmul(self.Wf, hx) + self.bf); # forget gate
        ig = self.gate(tf.matmul(self.Wi, hx) + self.bi); # input gate
        hg = self.gate(tf.matmul(self.Wh, hx) + self.bh); # hidden gate
        ''' update network state '''
        new_c = self.activation(tf.matmul(self.Wc, hx)+self.bc); # proposed new candidate values
        self.c = fg * self.c + ig * new_c;          # update candidate values
        self.h = hg * self.activation(self.c);          # update the hidden values
        self.o = self.activation_out(tf.squeeze(tf.matmul(self.Wo, self.h) + self.bo));  # update network output
        return self.o
    #####################################################################################################################
    #####################################################################################################################


    
    
    #####################################################################################################################    
    ''' update_state: reset the state to the network to the init values '''
    #####################################################################################################################
    def reset(self):
        self.h = tf.zeros(shape = [self.net_size, 1]);  # hidden state
        self.c = tf.zeros(shape = [self.net_size, 1]);  # new candidate state
        self.o = tf.zeros(shape = [self.out_size]);  # output state
    #####################################################################################################################
    #####################################################################################################################

    
    #####################################################################################################################    
    ''' run_system: run the system for multiple inputs'''
    #####################################################################################################################
    def run_system(self, X):
        o = [];
        for i in range(X.shape[0]):
            o +=[self.update_state(X[i, :])]
        return tf.pack(o);
    #####################################################################################################################
    #####################################################################################################################

    
#%% 
input_size = 256;
net_size = 500; 
out_size = 256;
net = lstm_net(input_size, net_size, out_size);
x = tf.placeholder(shape = [1, input_size], dtype = net.dtype); # input vector 
init = tf.global_variables_initializer()
#%%
sess = tf.InteractiveSession();
init.run()
#%% run net for input sequence;
X = rng.randn(100, out_size)

out = net.run_system(X);

